from flask import Flask, request, render_template,redirect

books = Flask(__name__)

class Book():
    title:str
    autor:str
    numberofpages:int
    genre:str
    publisher:str
    
    def __init__(self,title,autor,numberofpages,genre,publisher):
        self.title = title
        self.autor = autor
        self.numberofpages = numberofpages
        self.genre = genre
        self.publisher = publisher

libros = []

@books.route('/')
def Listbooks():
    return render_template('list.html', libros=libros)

@books.route('/newBook', methods=['GET','POST'])
def NewBook():
    if request.method == 'POST':
        if(request.form['title']is not None or request.form['author']is not None or request.form['numberOfPages']is not None or request.form['genre'] is not None or request.form['publisher'] is not None):
            title = request.form['title']
            autor = request.form['autor']
            numberofpages = request.form['numberOfPages']
            genre = request.form['genre']
            publisher = request.form['publisher']
            libro=Book(title=title, autor=autor, numberofpages=numberofpages, genre=genre, publisher=publisher)
            libros.append(libro)
        return redirect("http://localhost:5000/")
    if request.method == 'GET':
        return render_template('newBook.html')

@books.route('/delete/<int:num>', methods=['GET'])
def deleteBook(num):
    libros.pop(num-1)
    return redirect("http://localhost:5000/")


if __name__ == '__main__':
    books.run(debug=True)